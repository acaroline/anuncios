class Anuncio < ActiveRecord::Base
  validates :nombre_mascota,  presence: true,
                              length: { minimum: 2 }
  validates :nombre_cuidador,  presence: true,
                              length: { minimum: 2 }
end
