class AnunciosController < ApplicationController

def index
  @anuncios = Anuncio.all
end

def show
  @anuncio = Anuncio.find(params[:id])
end

def new
  @anuncio = Anuncio.new
end

def edit
  @anuncio = Anuncio.find(params[:id])
end

def create
  @anuncio = Anuncio.new(anuncio_params)

  if @anuncio.save
    redirect_to @anuncio
  else
    render 'new'
  end
end

def update
  @anuncio = Anuncio.find(params[:id])

  if @anuncio.update(anuncio_params)
    redirect_to @anuncio
  else
    render 'edit'
  end
end

def destroy
  @anuncio = Anuncio.find(params[:id])
  @anuncio.destroy

  redirect_to anuncios_path
end

private
  def anuncio_params
    params.require(:anuncio).permit(:nombre_mascota, :descripcion, :nombre_cuidador)
  end

end
