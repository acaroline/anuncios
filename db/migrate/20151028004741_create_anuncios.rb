class CreateAnuncios < ActiveRecord::Migration
  def change
    create_table :anuncios do |t|
      t.string :nombre_mascota
      t.text :descripcion
      t.string :nombre_cuidador

      t.timestamps
    end
  end
end
